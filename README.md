Un simple service Rest

springboot
maven

http://localhost:8080/greeting
{"id":1,"content":"Hello, World!"}

http://localhost:8080/greeting?name=User
{"id":1,"content":"Hello, User!"}
